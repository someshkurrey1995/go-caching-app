package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// handleGetKey method handler for get key by keyname
func handleGetKey(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	// Allow all origin to handle cors issue
	w.Header().Set("Access-Control-Allow-Origin", "*")
	params := mux.Vars(r)
	keys, err := GetKey(params["key"])
	if err != nil {
		errObj := ResponseObj{Data: nil, Message: err.Error(), Status: 500, Success: false}
		respondWithJSON(w, http.StatusInternalServerError, errObj)
		return
	}
	resObj := ResponseObj{Data: keys, Message: "Getting data successfully!", Status: 200, Success: true}
	respondWithJSON(w, http.StatusOK, resObj)
	return
}

// handleSetKey method handler for setting key
func handleSetKey(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	// set the header to content type x-www-form-urlencoded
	// Allow all origin to handle cors issue
	w.Header().Set("Context-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	// create an redis data
	var redisData RedisData

	// decode the json request to user
	err := json.NewDecoder(r.Body).Decode(&redisData)

	if err != nil {
		log.Fatalf("Unable to decode the request body.  %v", err)
	}

	// call set key function and pass the data
	keys, err := SetKey(redisData)
	if err != nil {
		errObj := ResponseObj{Data: nil, Message: err.Error(), Status: 500, Success: false}
		respondWithJSON(w, http.StatusInternalServerError, errObj)
		return
	}
	resObj := ResponseObj{Data: keys, Message: "Getting data successfully!", Status: 200, Success: true}
	respondWithJSON(w, http.StatusOK, resObj)
	return
}

// handleGetKeyAll method handler for search all keys
func handleGetKeyAll(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	suffix := r.FormValue("suffix")
	prefix := r.FormValue("prefix")
	var searchData SearchData
	if len(suffix) != 0 {
		searchData.Type = "suffix"
		searchData.Value = suffix
	}
	if len(prefix) != 0 {
		searchData.Type = "prefix"
		searchData.Value = prefix
	}
	keys := GetKeysAll(searchData)
	resObj := ResponseObj{Data: keys, Message: "Getting data successfully!", Status: 200, Success: true}
	respondWithJSON(w, http.StatusOK, resObj)
	return
}

// handleHealthCheck method
func handleHealthCheck(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	resObj := ResponseObj{Data: nil, Message: "Getting healthcheck successfully!", Status: 200, Success: true}
	respondWithJSON(w, http.StatusOK, resObj)
}

// method to print error output for http response
func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJSON(w, code, map[string]string{"error": msg})
}

// method to print output for http respon
// parameter  [w (Http.RestponWriter), http.statuscode, payload/data/msg]
//respondWithJSON payload is data which will be transfer to other part
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Write(response)
}
