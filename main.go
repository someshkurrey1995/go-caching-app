package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {

	var r = mux.NewRouter()
	r.HandleFunc("/get/{key}", handleGetKey).Methods("GET")
	r.HandleFunc("/search", handleGetKeyAll).Methods("GET")
	r.HandleFunc("/set", handleSetKey).Methods("POST", "OPTIONS")
	r.HandleFunc("/healthcheck", handleHealthCheck).Methods("GET")

	port := ":80" // port for run the app
	fmt.Println("Start listening on port", port)
	// Serve server on a port
	if err := http.ListenAndServe(port, r); err != nil {
		fmt.Println("Error running application")
	}
}
