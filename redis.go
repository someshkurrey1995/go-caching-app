package main

import (
	"fmt"
	"os"

	"github.com/go-redis/redis"
)

func redisConnection() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_URL"),
		Password: "",
		DB:       0,
	})
	return client
}

func GetKey(key string) (string, error) {
	client := redisConnection()
	// getting specific key by key name
	val, err := client.Get(key).Result()
	if err != nil {
		fmt.Println(err)
	}
	return val, err
}

func SetKey(redisData RedisData) (string, error) {
	client := redisConnection()
	// setting key & value
	val, err := client.Set(redisData.Key, redisData.Value, 0).Result()
	if err != nil {
		fmt.Println(err)
	}
	return val, err
}

func GetKeysAll(searchData SearchData) interface{} {
	client := redisConnection()
	var key string
	if searchData.Type == "prefix" {
		key = searchData.Value + "*"
	}
	if searchData.Type == "suffix" {
		key = "*" + searchData.Value
	}
	// getting all matching keys
	val, err := client.Do("KEYS", key).Result()
	if err != nil {
		fmt.Println(err)
	}
	return val
}
