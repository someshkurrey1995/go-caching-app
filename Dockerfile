FROM golang:latest as builder

WORKDIR /src
COPY . .

RUN go mod download
RUN go mod verify
RUN CGO_ENABLED=0 go build -tags netgo -a -v -o main *.go

FROM alpine:3.8

WORKDIR /app
RUN mkdir /app/caching-app
COPY --from=builder /src/main .

CMD [ "./main" ]