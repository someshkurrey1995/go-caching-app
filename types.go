package main

// Response type
type ResponseObj struct {
	Data    interface{} `bson:"data" json:"data"`
	Message string      `bson:"message" json:"message"`
	Status  int         `bson:"status" json:"status"`
	Success bool        `bson:"success" json:"success"`
}

// Redis data key value pair type
type RedisData struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// Query params data type
type SearchData struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}
