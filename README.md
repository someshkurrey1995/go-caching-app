# An in-memory key-value store HTTP API
Simple Rest API with **Go** and **redis**
## Build & Run

Build 
```bash
go build .
```
Run 
```bash
./caching-app (project-name)
```

## API

#### /get/{key}
* `GET` : Get the key value using key name

#### /set
* `POST` : Set key & value using json {"key": "", "value": ""}

#### /search?prefix=abc or /search?suffix=-1
* `GET` : Get all keys based on the search criteria

#### /healthcheck"
* `GET` : Get healthcheck of app



Build docker image:
docker build -t caching-app:1.0.0 .